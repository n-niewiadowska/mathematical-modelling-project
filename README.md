# Mathematical Modelling project

This is an analysis of stock's closing prices for Nexity Global and PKP Cargo, created as a university project for Mathematical Modelling. **RStudio** is needed to run the code.

### Tech stack

`R` `LaTex`

### Description

The project contains separate analysis of closing prices for both companies, log-returns analysis and linear regression analysis. It uses libraries like fitdistrplus, e1074, ggplot2 etc. 

`.tex` file presents results from `.r` script and their interpretation. The whole project is in Polish.
